from django.test import TestCase
from django.urls import reverse
from mixer.backend.django import mixer

from authors.models import Author
from books.models import Book
from users.models import MyUser


class BaseTestCase(TestCase):
    def setUp(self):
        self.test_user = mixer.blend(MyUser)
        self.test_user2 = mixer.blend(MyUser)
        self.staff_user = mixer.blend(MyUser, is_staff=True)
        self.author = mixer.blend(Author)
        self.book = mixer.blend(
            Book, author=self.author, created_by=self.test_user
        )

    def tearDown(self):
        print('Tear down')


class BookCreateViewTestCase(BaseTestCase):
    def test_get(self):
        self.client.force_login(self.test_user)  # log in as user
        url = reverse('book_create')
        response = self.client.get(url)
        expected_data1 = '<h1 class="content-title">Add Book Page</h1>'
        expected_data2 = f"""<option value="1">{self.author.first_name} {self.author.last_name}</option>"""
        self.assertIn(expected_data1.encode(), response.content)
        self.assertIn(expected_data2.encode(), response.content)
        self.assertTemplateUsed(response, 'books/book_create.html')
        self.assertEqual(response.status_code, 200)

        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)  # check logged out user has no access

        self.client.force_login(self.staff_user)  # log in as staff
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        self.client.force_login(self.test_user)
        self.assertEqual(1, Book.objects.all().count())
        url = reverse('book_create')
        fake_name = mixer.faker.name()
        data = {
            'name': fake_name,
            'publish_date': mixer.faker.small_positive_integer(),
            'author': self.author.pk
        }
        response = self.client.post(url, data=data)
        book = Book.objects.filter(name=fake_name)
        self.assertEqual(book.first().created_by, self.test_user)  # test if book user added
        self.assertEqual(book.first().author.pk, data['author'])  # test if book is created
        self.assertEqual(book.first().publish_date, int(data['publish_date']))
        self.assertEqual(response.status_code, 302)  # check if redirected
        self.assertEqual(2, Book.objects.all().count())


class BookDetailViewTestCase(BaseTestCase):
    def test_get(self):
        url = reverse('book', kwargs={'pk': self.book.pk})
        response = self.client.get(url)
        expected_data = f"""<p>Author: <a href="/authors/1">{self.author.first_name} {self.author.last_name}</a></p>\n    <p>Book: {self.book.name}</p>\n    <p>Publish date: {self.book.publish_date}</p>"""
        self.assertIn(expected_data.encode(), response.content)  # test if expected_data in content
        self.assertIn('object', response.context)  # test if object in context
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'books/book_detail.html')


class BookListViewTestCase(BaseTestCase):
    def test_get(self):
        url = reverse('book_list')
        response = self.client.get(url)
        expected_data1 = '<h1 class="content-title">Books Page</h1>'
        expected_data2 = f'<a href="/books/1">{self.book.name}</a>'
        self.assertIn(expected_data1.encode(), response.content)  # test if expected_data in content
        self.assertIn(expected_data2.encode(), response.content)  # test if expected_data in content
        self.assertIn('object_list', response.context)  # test if object_list in context
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'books/book_list.html')


class BookUpdateViewTestCase(BaseTestCase):
    def test_get(self):
        self.client.force_login(self.test_user)  # log in user
        url = reverse('book_update', kwargs={'pk': self.book.pk})
        response = self.client.get(url)
        expected_data = f"""<input type="text" name="name" value="{self.book.name}" maxlength="100" required id="id_name">"""
        self.assertIn(expected_data.encode(), response.content)  # check if book update form is correct
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'books/book_update.html')

    def test_get_with_user_logged_out(self):
        url = reverse('book_update', kwargs={'pk': self.book.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.assertTemplateNotUsed(response, 'books/book_update.html')

    def test_update_with_created_by_user(self):  # test with user who created the book
        self.client.force_login(self.test_user)
        url = reverse('book_update', kwargs={'pk': self.book.pk})
        data = {
            'name': mixer.faker.name(),
            'publish_date': mixer.faker.small_positive_integer(),
            'author': self.author.pk
        }
        response = self.client.post(url, data)
        self.book.refresh_from_db()
        self.assertEqual(data['name'], self.book.name)  # check if data is updated
        self.assertEqual(data['publish_date'], self.book.publish_date)
        self.assertEqual(response.status_code, 302)  # check if redirects

    def test_update_with_not_created_by_user(self):  # test with user who not created the book
        self.client.force_login(self.test_user2)
        url = reverse('book_update', kwargs={'pk': self.book.pk})
        data = {
            'name': mixer.faker.name(),
            'publish_date': mixer.faker.small_positive_integer(),
            'author': self.author.pk
        }
        response = self.client.post(url, data)
        self.book.refresh_from_db()
        self.assertNotEqual(data['name'], self.book.name)  # check if data is not updated
        self.assertNotEqual(data['publish_date'], self.book.publish_date)
        self.assertEqual(response.status_code, 403)  # check if redirects

    def test_update_user_not_owner_but_staff(self):
        self.client.force_login(self.staff_user)  # log in staff
        url = reverse('book_update', kwargs={'pk': self.book.pk})
        data = {
            'name': mixer.faker.name(),
            'publish_date': mixer.faker.small_positive_integer(),
            'author': self.author.pk
        }
        response = self.client.post(url, data)
        self.book.refresh_from_db()
        self.assertEqual(data['name'], self.book.name)  # check if data is updated
        self.assertEqual(data['publish_date'], self.book.publish_date)
        self.assertEqual(response.status_code, 302)  # check if redirects


class BookDeleteViewTestCase(BaseTestCase):
    def test_delete(self):  # test delete the book by the user who created the book
        self.assertEqual(1, Book.objects.all().count())
        self.client.force_login(self.test_user)
        url = reverse('book_delete', kwargs={'pk': self.book.pk})
        response = self.client.delete(url)
        self.assertIsNone(Book.objects.filter(id=self.book.id).first())  # check if deleted
        self.assertEqual(response.status_code, 302)  # check if redirects
        self.assertEqual(0, Book.objects.all().count())

    def test_delete_staff_logged_in(self):  # test logged in staff can delete a book
        self.assertEqual(1, Book.objects.all().count())
        self.client.force_login(self.staff_user)
        url = reverse('book_delete', kwargs={'pk': self.book.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 302)  # check if redirects
        self.assertIsNone(Book.objects.filter(id=self.book.id).first())  # check if deleted
        self.assertEqual(0, Book.objects.all().count())

    def test_delete_not_created_by_the_user(self):  # test user who not created the book cannot delete
        self.assertEqual(1, Book.objects.all().count())
        self.client.force_login(self.test_user2)
        url = reverse('book_delete', kwargs={'pk': self.book.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 403)  # check if redirects
        self.assertIsNotNone(Book.objects.filter(id=self.book.id).first())  # check if book exists
        self.assertEqual(1, Book.objects.all().count())
