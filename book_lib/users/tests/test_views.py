from django.test import TestCase
from django.urls import reverse
from faker import Faker

from users.models import MyUser

fake = Faker()


class BaseTestCase(TestCase):
    def setUp(self):
        self.fake_username = fake.simple_profile()['username']
        self.password = fake.pystr_format(string_format='???{{random_int}}?#?#?#')
        self.test_user = MyUser.objects.create_user(username=self.fake_username, password=self.password)

    def tearDown(self):
        print('Tear down')


class RegisterTestCase(BaseTestCase):
    def test_get(self):
        url = reverse('register')
        response = self.client.get(url)
        expected_data = '<h1 class="content-title">Registration Page</h1>'
        self.assertIn(expected_data.encode(), response.content)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/register.html')

    def test_get_with_user_logged_in(self):
        url = reverse('register')
        self.client.force_login(self.test_user)  # check logged in user has no access
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_post(self):
        self.assertEqual(1, MyUser.objects.all().count())
        url = reverse('register')
        fake_username = fake.simple_profile()['username']
        password = fake.pystr_format(string_format='???{{random_int}}?#?#?#')
        data = {
            'username': fake_username,
            'email': fake.email(),
            'password1': password,
            'password2': password,
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
        }
        response = self.client.post(url, data=data)
        new_user = MyUser.objects.filter(username=fake_username)
        self.assertEqual(new_user.first().username, fake_username)  # test if user created
        self.assertEqual(2, MyUser.objects.all().count())
        self.assertEqual(response.status_code, 302)  # check if redirected


class LoginTestCase(BaseTestCase):

    def test_get(self):
        url = reverse('login')
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users/login.html')

    def test_get_with_user_logged_in(self):
        url = reverse('login')
        self.client.force_login(self.test_user)  # check logged in user has no access
        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_post(self):
        url = reverse('login')
        data = {
            'username': self.fake_username,
            'password': self.password,
        }
        response = self.client.get(url, data)
        self.assertEqual(response.status_code, 200)
