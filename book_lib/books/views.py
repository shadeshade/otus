from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    DeleteView,
    UpdateView
)

from .forms import BookCreateViewForm
from .models import Book


class BookCreateView(LoginRequiredMixin, CreateView):
    model = Book
    template_name = 'books/book_create.html'
    form_class = BookCreateViewForm
    success_url = reverse_lazy('book_list')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)


class BookDetailView(DetailView):
    model = Book
    template_name = 'books/book_detail.html'


class BookListView(ListView):
    model = Book
    template_name = 'books/book_list.html'
    paginate_by = 25


class BookUpdateView(UserPassesTestMixin, UpdateView):
    model = Book
    template_name = 'books/book_update.html'
    fields = ['name', 'publish_date', 'author', ]

    def test_func(self):
        book = self.get_object()
        user = self.request.user
        return book.created_by == user or user.is_staff

    def get_success_url(self):
        """Return the URL to redirect to after processing a valid form."""
        return reverse_lazy('book', kwargs={'pk': self.kwargs['pk']})


class BookDeleteView(UserPassesTestMixin, DeleteView):
    model = Book
    template_name = 'delete_confirm.html'
    success_url = reverse_lazy('book_list')

    def test_func(self):
        book = self.get_object()
        user = self.request.user
        return book.created_by == user or user.is_staff
