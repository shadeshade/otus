from django.test import SimpleTestCase
from django.urls import reverse, resolve

from users.views import (
    Register,
    Login,
    Logout
)


class UsersUrlsSimpleTestCase(SimpleTestCase):

    def test_register_resolves(self):
        url = reverse('register')
        self.assertEqual(resolve(url).func.view_class, Register)

    def test_login_resolves(self):
        url = reverse('login', )
        self.assertEqual(resolve(url).func.view_class, Login)

    def test_logout_resolves(self):
        url = reverse('logout')
        self.assertEqual(resolve(url).func.view_class, Logout)
