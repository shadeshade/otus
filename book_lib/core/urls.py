from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from .views import index_view, about_view

# app_name = 'core'

urlpatterns = [
    path('', index_view, name='index'),
    path('about', about_view, name='about'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
