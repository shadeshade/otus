from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from .views import (
    AuthorDetailView,
    AuthorDeleteView,
    AuthorListView,
    AuthorCreateView,
    AuthorUpdateView
)

# app_name = 'authors'

urlpatterns = [
    path('', AuthorListView.as_view(), name='author_list'),
    path('<int:pk>', AuthorDetailView.as_view(), name='author'),
    path('create/', AuthorCreateView.as_view(), name='author_create'),
    path('update/<int:pk>/', AuthorUpdateView.as_view(), name='author_update'),
    path('delete/<int:pk>/', AuthorDeleteView.as_view(), name='author_delete'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
