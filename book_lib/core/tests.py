from django.test import SimpleTestCase
from django.urls import reverse


class ViewsSimpleTestCase(SimpleTestCase):

    def test_index_view_GET(self):
        url = reverse('index')
        response = self.client.get(url)
        expected_data = """<h1>index page</h1>"""

        self.assertIn(expected_data.encode(), response.content)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_about_view_GET(self):
        url = reverse('about')
        response = self.client.get(url)
        expected_data = """<h1 class="content-title">About Page</h1>"""

        self.assertIn(expected_data.encode(), response.content)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'about.html')
