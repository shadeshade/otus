from django.test import TestCase
from django.urls import reverse
from faker import Faker

from authors.models import Author
from authors.tests.factories import UserFactory, AuthorFactory

fake = Faker()


class BaseTestCase(TestCase):
    def setUp(self):
        self.test_user = UserFactory()
        self.staff_user = UserFactory(is_staff=True)
        self.author = AuthorFactory()
        self.author2 = AuthorFactory(created_by=self.test_user)

    def tearDown(self):
        print('Tear down')


class AuthorCreateViewTestCase(BaseTestCase):
    def test_get(self):
        self.client.force_login(self.test_user)  # log in as user
        url = reverse('author_create')
        response = self.client.get(url)
        expected_data = '<h1 class="content-title">Add Author Page</h1>'
        self.assertIn(expected_data.encode(), response.content)
        self.assertTemplateUsed(response, 'authors/author_create.html')
        self.assertEqual(response.status_code, 200)  # check logged in user has access

        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)  # check logged out user has no access

        self.client.force_login(self.staff_user)  # log in as staff
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_post(self):
        self.client.force_login(self.test_user)
        self.assertEqual(2, Author.objects.all().count())
        url = reverse('author_create')
        fake_name = fake.first_name()
        data = {
            'first_name': fake_name,
            'last_name': fake.last_name(),
            'date_of_birth': fake.date_of_birth(),
        }
        response = self.client.post(url, data=data)
        author = Author.objects.filter(first_name=fake_name)
        self.assertEqual(author.first().created_by, self.test_user)  # test if author user added
        self.assertEqual(response.status_code, 302)  # check if redirected
        self.assertEqual(3, Author.objects.all().count())


class AuthorDetailViewTestCase(BaseTestCase):
    def test_get(self):
        url = reverse('author', kwargs={'pk': self.author.pk})
        response = self.client.get(url)
        expected_data = '<h1 class="content-title">Author Page</h1>'
        self.assertIn(expected_data.encode(), response.content)  # test if expected_data in content
        self.assertIn('object', response.context)  # test if object in context
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'authors/author_detail.html')


class AuthorListViewTestCase(BaseTestCase):
    def test_get(self):
        url = reverse('author_list')
        response = self.client.get(url)
        expected_data = '<h1 class="content-title">Author List Page</h1>'
        self.assertIn(expected_data.encode(), response.content)  # test if expected_data in content
        self.assertIn('object_list', response.context)  # test if object_list in context
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'authors/author_list.html')


class AuthorUpdateViewTestCase(BaseTestCase):
    def test_get(self):
        self.client.force_login(self.test_user)  # log in user
        url = reverse('author_update', kwargs={'pk': self.author2.pk})
        response = self.client.get(url)
        expected_data = '<h1 class="content-title">Author Update Page</h1>'
        self.assertIn(expected_data.encode(), response.content)  # check if author update form is correct
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'authors/author_update.html')

    def test_get_with_user_logged_out(self):
        url = reverse('author_update', kwargs={'pk': self.author.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)
        self.assertTemplateNotUsed(response, 'authors/author_update.html')

    def test_update_with_created_by_user(self):  # test with user who created the author
        self.client.force_login(self.test_user)
        url = reverse('author_update', kwargs={'pk': self.author2.pk})
        data = {
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'date_of_birth': fake.date_of_birth(),
        }
        response = self.client.post(url, data)
        self.author2.refresh_from_db()
        self.assertEqual(data['first_name'], self.author2.first_name)  # check if data is updated
        self.assertEqual(data['date_of_birth'], self.author2.date_of_birth)
        self.assertEqual(response.status_code, 302)  # check if redirects

    def test_update_with_not_created_by_user(self):  # test with user who not created the author
        self.client.force_login(self.test_user)
        url = reverse('author_update', kwargs={'pk': self.author.pk})
        data = {
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'date_of_birth': fake.date_of_birth(),
        }
        response = self.client.post(url, data)
        self.author.refresh_from_db()
        self.assertNotEqual(data['first_name'], self.author.first_name)  # check if data is not updated
        self.assertNotEqual(data['date_of_birth'], self.author.date_of_birth)
        self.assertEqual(response.status_code, 403)  # check if redirects

    def test_update_user_not_owner_but_staff(self):
        self.client.force_login(self.staff_user)  # log in staff
        url = reverse('author_update', kwargs={'pk': self.author.pk})
        data = {
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'date_of_birth': fake.date_of_birth(),
        }
        response = self.client.post(url, data)
        self.author.refresh_from_db()
        self.assertEqual(data['first_name'], self.author.first_name)  # check if data is updated
        self.assertEqual(data['date_of_birth'], self.author.date_of_birth)
        self.assertEqual(response.status_code, 302)  # check if redirects


class AuthorDeleteViewTestCase(BaseTestCase):
    def test_delete(self):  # test delete the author by the user who created the author
        self.assertEqual(2, Author.objects.all().count())
        self.client.force_login(self.test_user)
        url = reverse('author_delete', kwargs={'pk': self.author2.pk})
        response = self.client.delete(url)
        self.assertIsNone(Author.objects.filter(id=self.author2.id).first())  # check if deleted
        self.assertEqual(response.status_code, 302)  # check if redirects
        self.assertEqual(1, Author.objects.all().count())

    def test_delete_staff_logged_in(self):  # test logged in staff can delete a author
        self.assertEqual(2, Author.objects.all().count())
        self.client.force_login(self.staff_user)
        url = reverse('author_delete', kwargs={'pk': self.author2.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 302)  # check if redirects
        self.assertIsNone(Author.objects.filter(id=self.author2.id).first())  # check if deleted
        self.assertEqual(1, Author.objects.all().count())

    def test_delete_not_created_by_the_user(self):  # test user who not created the author cannot delete
        self.assertEqual(2, Author.objects.all().count())
        self.client.force_login(self.test_user)
        url = reverse('author_delete', kwargs={'pk': self.author.pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, 403)  # check if redirects
        self.assertIsNotNone(Author.objects.filter(id=self.author.id).first())  # check if author exists
        self.assertEqual(2, Author.objects.all().count())
