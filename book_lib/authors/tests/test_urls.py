from django.test import SimpleTestCase
from django.urls import reverse, resolve

from authors.views import (
    AuthorDetailView,
    AuthorDeleteView,
    AuthorListView,
    AuthorCreateView,
    AuthorUpdateView
)


class AuthorsUrlsSimpleTestCase(SimpleTestCase):

    def test_author_list_resolves(self):
        url = reverse('author_list')
        self.assertEqual(resolve(url).func.view_class, AuthorListView)

    def test_author_resolves(self):
        url = reverse('author', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, AuthorDetailView)

    def test_author_create_resolves(self):
        url = reverse('author_create')
        self.assertEqual(resolve(url).func.view_class, AuthorCreateView)

    def test_author_update_resolves(self):
        url = reverse('author_update', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, AuthorUpdateView)

    def test_author_delete_resolves(self):
        url = reverse('author_delete', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, AuthorDeleteView)
