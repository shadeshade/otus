from django.test import SimpleTestCase
from django.urls import reverse, resolve

from books.views import (
    BookListView,
    BookDetailView,
    BookCreateView,
    BookUpdateView,
    BookDeleteView
)


class BooksUrlsSimpleTestCase(SimpleTestCase):

    def test_book_list_resolves(self):
        url = reverse('book_list')
        self.assertEqual(resolve(url).func.view_class, BookListView)

    def test_book_resolves(self):
        url = reverse('book', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, BookDetailView)

    def test_book_create_resolves(self):
        url = reverse('book_create')
        self.assertEqual(resolve(url).func.view_class, BookCreateView)

    def test_book_update_resolves(self):
        url = reverse('book_update', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, BookUpdateView)

    def test_book_delete_resolves(self):
        url = reverse('book_delete', kwargs={'pk': 1})
        self.assertEqual(resolve(url).func.view_class, BookDeleteView)
