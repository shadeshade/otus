from django.conf import settings
from django.db import models


class Author(models.Model):

    class Meta:
        ordering = ["-id"]

    first_name = models.CharField(max_length=20, null=False)
    last_name = models.CharField(max_length=20, null=False)
    date_of_birth = models.DateField(max_length=8, null=True, blank=True)
    date_of_death = models.DateField(max_length=8, null=True, blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='added_authors',
        on_delete=models.SET_NULL,
        null=True,
        blank=True
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
