from django.test import TestCase
from mixer.backend.django import mixer

from authors.models import Author
from books.forms import BookCreateViewForm


class BooksFormsSimpleTestCase(TestCase):

    def test_form_is_valid(self):
        form = BookCreateViewForm(data={
            'name': mixer.faker.name(),
            'publish_date': mixer.faker.pyint(max_value=2000),
            'author': mixer.blend(Author),
        })
        self.assertTrue(form.is_valid())

    def test_form_no_data(self):
        form = BookCreateViewForm()

        self.assertFalse(form.is_valid())
