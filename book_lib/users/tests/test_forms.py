from django.test import TestCase
from faker import Faker

from users.forms import MyUserCreateForm

fake = Faker()


class UsersFormsTestCase(TestCase):

    def test_form_is_valid(self):
        password = fake.pystr_format(string_format='???{{random_int}}?#?#?#')

        form = MyUserCreateForm(data={
            'username': fake.simple_profile()['username'],
            'email': fake.email(),
            'password1': password,
            'password2': password,
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
        })

        self.assertTrue(form.is_valid())

    def test_form_no_data(self):
        form = MyUserCreateForm()

        self.assertFalse(form.is_valid())
