from django.db import models
from django.conf import settings


class Book(models.Model):
    class Meta:
        unique_together = ('name', 'author')
        ordering = ["-id"]

    name = models.CharField(max_length=100, null=False)
    publish_date = models.PositiveSmallIntegerField(null=True, blank=True)
    author = models.ForeignKey(
        'authors.Author',
        related_name='books',
        on_delete=models.CASCADE
    )
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='added_books',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    def __str__(self):
        return f"{self.id} Book: {self.name} Author: {self.author}"
