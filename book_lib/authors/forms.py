import datetime

from django.forms import ModelForm, forms

from .models import Author


class AuthorCreateViewForm(ModelForm):
    class Meta:
        model = Author

        fields = [
            'first_name',
            'last_name',
            'date_of_birth',
            'date_of_death',
        ]

    def clean(self):
        cleaned_data = super().clean()
        d1 = cleaned_data.get("date_of_birth")
        d2 = cleaned_data.get("date_of_death")
        if d1 and d2:
            if datetime.date(d1.year, d1.month, d1.day) > datetime.date(d2.year, d2.month, d2.day):
                raise forms.ValidationError('Date of birth cannot be after Date of death')

        return cleaned_data
