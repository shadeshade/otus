from django.core.management.base import BaseCommand

from authors.tests.factories import UserFactory, AuthorFactory, BookFactory


class Command(BaseCommand):
    def handle(self, *args, **options):
        for _ in range(10):
            user = UserFactory()
            author = AuthorFactory(created_by=user)
            BookFactory.create_batch(3, created_by=user, author=author)
