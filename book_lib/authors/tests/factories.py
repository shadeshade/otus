import datetime

import factory
from factory import fuzzy


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'users.MyUser'

    username = factory.lazy_attribute(lambda x: f'{x.first_name}{x.last_name}')
    email = factory.lazy_attribute(lambda x: f'{x.username}.@test.com'.lower())
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')


class AuthorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'authors.Author'

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    date_of_birth = fuzzy.FuzzyDate(datetime.date(1000, 1, 1), datetime.date(1799, 1, 1))
    date_of_death = fuzzy.FuzzyDate(datetime.date(1800, 1, 1), datetime.date(2000, 1, 1))
    created_by = factory.SubFactory(UserFactory)


class BookFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'books.Book'

    name = factory.Faker('name')
    publish_date = factory.fuzzy.FuzzyInteger(1900, 2000)
    author = factory.SubFactory(AuthorFactory)
    created_by = factory.SubFactory(UserFactory)
