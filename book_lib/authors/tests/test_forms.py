from django.test import SimpleTestCase
from faker import Faker
from authors.forms import AuthorCreateViewForm

fake = Faker()


class AuthorsFormsSimpleTestCase(SimpleTestCase):

    def test_form_is_valid(self):
        form = AuthorCreateViewForm(data={
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'date_of_birth': fake.date_of_birth(minimum_age=40, maximum_age=99),
            'date_of_death': fake.date_between(start_date='-40y', end_date='today'),
        })

        self.assertTrue(form.is_valid())

    def test_form_not_valid(self):
        form = AuthorCreateViewForm(data={
            'first_name': fake.first_name(),
            'last_name': fake.last_name(),
            'date_of_birth': fake.date_between(start_date='-40y', end_date='today'),
            'date_of_death': fake.date_of_birth(minimum_age=40, maximum_age=99),
        })

        self.assertFalse(form.is_valid())

    def test_form_no_data(self):
        form = AuthorCreateViewForm()

        self.assertFalse(form.is_valid())
