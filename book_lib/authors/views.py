from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    DeleteView,
    UpdateView
)

from .forms import AuthorCreateViewForm
from .models import Author


class AuthorCreateView(LoginRequiredMixin, CreateView):
    model = Author
    template_name = 'authors/author_create.html'
    form_class = AuthorCreateViewForm
    success_url = reverse_lazy('author_list')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super().form_valid(form)
    

class AuthorDetailView(DetailView):
    model = Author
    template_name = 'authors/author_detail.html'


class AuthorListView(ListView):
    model = Author
    template_name = 'authors/author_list.html'


class AuthorUpdateView(UserPassesTestMixin, UpdateView):
    model = Author
    template_name = 'authors/author_update.html'
    fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_death', ]

    def test_func(self):
        author = Author.objects.get(id=self.kwargs['pk'])
        user = self.request.user
        return author.created_by == user or user.is_staff

    def get_success_url(self):
        """Return the URL to redirect to after processing a valid form."""
        return reverse_lazy('author', kwargs={'pk': self.kwargs['pk']})


class AuthorDeleteView(UserPassesTestMixin, DeleteView):
    model = Author
    template_name = 'delete_confirm.html'
    success_url = reverse_lazy('author_list')

    def test_func(self):
        author = Author.objects.get(id=self.kwargs['pk'])
        user = self.request.user
        return author.created_by == user or user.is_staff
