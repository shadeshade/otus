from django.forms import ModelForm

from .models import Book


class BookCreateViewForm(ModelForm):
    class Meta:
        model = Book

        fields = [
            'name',
            'publish_date',
            'author',
        ]
